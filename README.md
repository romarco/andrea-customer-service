# Andrea Customer Service

## Provide OpenAI and EleveLabs Api keys

To use this demo you should provide your API keys.

Copy the [.example.env](secrets/env.example) to your local `secrets/.env` path and modify the API keys environment variables.

The service will automatically detect them.

## Run the customer service demo

The customer service demo can be run by executing the following command:

```bash
./start.sh
```

Then connect to [localhost:8010](http://localhost:8010) in your local browser.
