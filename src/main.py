import os
import json

from dotenv import load_dotenv
load_dotenv("secrets/.env")


import openai
from elevenlabs import play
from elevenlabs.client import ElevenLabs
import streamlit as st

class LLM:
    def __init__(self, model='gpt-3.5-turbo-0125'):
        with open("src/db.json", "r") as f:
            self.db_json = json.load(f)
        self.model = model
        self.company_sector_description = """Contact lenses manufacturer.
Provide a wide scale of products targeting specific user needs. Optics are our only costumers.
"""
        self.topics_to_route = [
            "the customer wants to return a product",
            "the customer asks for a technical question about a product"
        ]
        self.prompt = f"""
You are an assistant of the customer service call center of a company working in the following sector: {self.company_sector_description}.
You should answer to the questions going straight to the point to provide the user to a solution to his
problem. Remember you MUST always be the most polite as possible in your answers.

The following list represent your tasks:

- Identify the client by asking his client ID, if the client ID is not in the db you should prompt again for a valid ID instead.
- When you identify the unique customer you should NEVER give information about other customers
- Identify the client's problem
- Understand if the problem falls under the following category: {self.topics_to_route}
- If your answer to the previous question is yes, you should not go further and tell the customer that you are going to route the call to a specialized human operator
- If your answer to the previous question was no, you should try to satisfy the customer needs and find a solution to the problem.
- A solution is read only when the customer says he is satisfied

Remember that you should be as quick as possible to identify and solve the problem.
In case you feel you have not satisfied the customer needs you can propose as a
final attempt to route the call to a technical customer operator if they are available at that hour
otherwise just tell the opening times.

In addition to the previous instruction you might want to read the customer and order database to understand the orders and customer details.
Find the orders data here:
{self.db_json}
"""
        self.messages = [{"role": "system", "content": self.prompt}]

    def generate_response(self, temperature):
        response = openai.chat.completions.create(
            model=self.model,
            messages=[self.messages[0]] + self.messages[-4:],
            temperature=temperature,
        )
        return response

    def get_response_text(self, message, temperature=0): # use max_tokens pram if you need it
        question = {"role": "user", "content": message}
        self.messages.append(question)
        response = self.generate_response(temperature)
        response_text = response.choices[0].message.content.strip()
        self.messages.append({"role": "assistant", "content": response_text}) # uncomment if you do not need memory

        return response_text


def session_state_init():
    """Session state variables initialization"""
    
    if "llm_model" not in st.session_state:
        st.session_state["llm_model"] = LLM()

    if "text_to_speech_model" not in st.session_state:
        st.session_state["text_to_speech_model"] = ElevenLabs(
        api_key=os.getenv("XI_API_KEY"),
    )
        
    if "messages" not in st.session_state:
        st.session_state.messages = []
    
    if "voice_enabled" not in st.session_state:
        st.session_state["voice_enabled"] = False


def elevenlabs_generate(client, text):
    audio = client.generate(
        text=text,
        voice="James",
        model="eleven_multilingual_v2"
    )
    return audio


def play_response(response):
    audio = elevenlabs_generate(st.session_state["text_to_speech_model"], response)
    play(audio)


session_state_init()

st.title(":telephone_receiver: Andrea, 24/7 customer service")

st.session_state["voice_enabled"] = st.checkbox("Voice assistant")

for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

if prompt := st.chat_input("What is up?"):
    st.session_state.messages.append({"role": "user", "content": prompt})
    with st.chat_message("user"):
        st.markdown(prompt)
    with st.chat_message("assistant"):
        with st.spinner('Waiting for response'):
            response = st.session_state["llm_model"].get_response_text(prompt)
            if st.session_state["voice_enabled"]:
                play_response(response)
        st.markdown(response)
    st.session_state.messages.append({"role": "assistant", "content": response})
